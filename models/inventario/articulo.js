var mongoose = require('mongoose');

var ArticuloSchema = mongoose.Schema({
  nombre: {type: String},
  codigo: {type: String},
  descripcion: {type: String},
  existencia: {type: String, default: 0},
  precioCosto: {type: Number, default: 0 },
  precioVenta: {type: Number, default: 0},
  estado: {type: Boolean, default: true},
  proveedor: {
    proveedorId: {type: mongoose.Schema.Types.ObjectId},
    nombre: {type: String}
  }
});

var Articulo = module.exports = mongoose.model('Articulo', ArticuloSchema);

module.exports.createArticulo = function(newArticulo, callback){
  newArticulo.save(callback);
}

module.exports.getArticulos = function(callback){
  Articulo.find(callback);
}

module.exports.getArticuloById= function(Id, callback){
  Articulo.findById(Id, callback);
}

module.exports.updateArticulo= function(Id, cantidad, costo, callback){
  var condicion = { _id : Id};
  var update = { $set: { existencia: cantidad, precioCosto: costo }};
  var opciones = { upsert: true };
  Articulo.update( condicion, update, opciones, callback );
}
