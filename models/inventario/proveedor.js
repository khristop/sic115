var mongoose = require('mongoose');

var ProveedorSchema = mongoose.Schema({
  nombre: {type: String},
  telefono: {type: String},
  contacto: {type: String}
});

var Proveedor = module.exports = mongoose.model('Proveedor', ProveedorSchema);

module.exports.getProveedorById = function(id, callback){
  Proveedor.findById(id, callback);
}

module.exports.createProveedor = function(newProveedor, callback){
  newProveedor.save(callback);
}

module.exports.getProveedores= function(callback){
  Proveedor.find(callback);
}
