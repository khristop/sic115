var mongoose = require('mongoose');

var PrestacionSchema = mongoose.Schema({
  nombre: {type:String},
  descripcion: { type:String},
  factor:{type:Number},
  obligatoria:{ type: Boolean, default: true},
  estado: {type: Boolean, default: true}
});

var Prestacion = module.exports = mongoose.model('Prestacion', PrestacionSchema);

module.exports.getPrestaciones = function(callback){
  Prestacion.find(callback);
}

module.exports.createPrestacion = function(newPrestacion, callback){
  newPrestacion.save(callback);
}
