  var mongoose = require('mongoose');

  var EmpleadoSchema = mongoose.Schema({
    nombre: {type: String},
    dui: {type: String},
    direccion: {type:String},
    fecha_nacimiento: {type: Date},
    nivel_academico: {type: String},
    situacion: {type:String},
    direccion: {type:String},
    email: {type:String},
    telefono: {type: String},
    codigo: {type: String},
    puesto: {type:String},
    horas_mes: {type: Number},
    salario: { type:Number},
    prestacionesExtra: [{
      nombre: {type: String},
      _id: mongoose.Schema.Types.ObjectId
    }]
});

var Empleado = module.exports = mongoose.model('Empleado', EmpleadoSchema);

module.exports.createEmpleado = function(newEmpleado, callback){
  newEmpleado.save(callback);
}

module.exports.getEmpleados = function(callback){
  Empleado.find(callback);
}
