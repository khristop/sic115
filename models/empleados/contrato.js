var express = require('mongoose');

var Contrato = mongoose.Schema({
  descripcion: {type: String},
  salario: {type: Number},
  telefono: {type: String},
  horario: {[
    inicio: {type: Date},
    fin: {type: Date}
  ]}
});

var Contrato = module.exports = mongoose.model('Contrato', ContratoSchema);
