//Dependencias
var mongoose = require('mongoose');

//model de tipos de pago

var PagoSchema = mongoose.Schema({
  tipo:{ type: String },
  codigo:{ type: Number, default: 1 },
  fechaDePago:{ type: [Date] },
  fechaDeCargo:{ type: [Date], default: Date.now },
  recurrencia: {
    periodo : {type: Number, default: 0},
    estado : {type: Boolean, default: false}
  }
});
// exportar el modelo
var Pago = module.exports = mongoose.model('Pago', PagoSchema);
