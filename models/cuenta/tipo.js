var mongoose = require('mongoose');

var TipoSchema = mongoose.Schema({
  nombre: {
    type: String
  },
  descripcion: {
    type: String
  },
  codigo: {
    type: String
  }
});

var Tipo = module.exports = mongoose.model('Tipo', TipoSchema);

module.exports.getTipos = function(callback){
  Tipo.find(callback);
}
