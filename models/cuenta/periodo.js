var mongoose = require('mongoose');

//modelos requeridos

//Modelo de tranasacciones
var PeriodoSchema = mongoose.Schema({
  fecha_inicio: { type: Date },
  fecha_fin: { type: Date },
  // cuentasActivas :[{
  //   cuenta_id: { type: [mongoose.Schema.Types.ObjectId]},
  //   montoCuenta: { type: Number}
  // }],
  notas : { type: String},
  estado: { type: Boolean, default: false}
});

var Periodo = module.exports = mongoose.model('Periodo', PeriodoSchema);

module.exports.iniciarPeriodo = function(neWperiodo, callback){
  neWperiodo.save(callback);
}
module.exports.periodoActivo = function( callback ){
  Periodo.find({ estado: true}, callback );
}
