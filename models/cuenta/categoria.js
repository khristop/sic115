var mongoose = require('mongoose');

var CategoriaSchema = mongoose.Schema({
  nombre: { type: String },
  codigo: { type: String, index: true },
  descripcion: { type: String},
  nivel: { type: Number },
  subcategorias: [{
      subCategoria_id: { type: [mongoose.Schema.Types.ObjectId] },
      categotia_nombre: {type: String}
  }],

  cuentas: [{
    cuenta_id: { type: [mongoose.Schema.Types.ObjectId] },
    cuenta_nombre: { type: String}
  }]
});


var Categoria = module.exports = mongoose.model('Categoria', CategoriaSchema);

module.exports.createCategoria = function(info, callback){
  nombre = info['nombre'];
  codigo = info['codigo'];
  categoriaPadre = info['padre'];
  var newCategoria = new Categoria({
    nombre: nombre,
    codigo: codigo
  });
  if(categoriaPadre != null && categoriaPadre != 0){
    var query = {'codigo': categoriaPadre};
    console.log(categoriaPadre);
    Categoria.findOneAndUpdate(
      query,
      {$push: {"subcategorias": {subCategoria_id: newCategoria.id, categotia_nombre: newCategoria.nombre}}},
      {safe: true, upsert: true},
      callback
  );
  }
  newCategoria.save(callback);
  // var padre = Categoria.getCategoriaById(categoriaPadre, callback){};
  // Categoria.findOneAndUpdate(
  //   categoriaPadre,
  //   {$push: {'subcategorias': {}}}
  // )
}

module.exports.getCategorias = function(callback){
  Categoria.find(callback);
}

module.exports.getCategoriaById = function(id, callback){
  Categoria.findOne(id, callback);
}
