var mongoose = require('mongoose');

//modelos requeridos

//Modelo de tranasacciones
var TransaccionSchema = mongoose.Schema({
  detalle:{ type: String},
  tipo:{ type: String},
  fecha_creacion: { type: Date },
  monto: { type: Number },
  tipoPago : { type: Number, default: 1}, //pago_id: {type: [mongoose.Schema.Types.ObjectId]} },
  //fecha_pago: { type: Date},
  cuentasAfectadas :[{
    _id: { type: mongoose.Schema.Types.ObjectId},
    nombre : { type: String},
    codigo: { type: String},
    Debe: { type: Number},
    Haber: { type: Number}
  }],
});

var Transaccion = module.exports = mongoose.model('Transaccion', TransaccionSchema);

module.exports.guardarTransaccion = function(newTransaccion, callback){
  newTransaccion.save(callback);
}

module.exports.getTransacciones = function(callback){
  Transaccion.find(callback);
}
