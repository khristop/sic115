var mongoose = require('mongoose');

var CuentaSchema = mongoose.Schema({
  nombre: { type: String }, //req
  descripcion: { type: String }, //req
  codigo: { type: String },
  estado:{ type: Boolean, default: false },
  //nivel :{ type: Number},
  tipoCuenta: { //req
    //type: [mongoose.Schema.Types.ObjectId]
    type : String
  },

  cuentaPadre: { //req
    padre_id: { type: String },//[mongoose.Schema.Types.ObjectId]},
    padre_nombre: { type : String }
  },
  saldo: {
    saldoDebe : { type: Number, default: 0},
    saldoHaber : { type: Number, default: 0}
  },
  desc_cuenta: [{
    //fechas
    fechaInicial: { type:[Date]},
    fechaFinal: { type:[Date]},
    //saldos
    saldoDebe : { type: Number, default: 0},
    saldoHaber : { type: Number, default: 0},
    transacciones: [{
        transaccion_id: { type: [mongoose.Schema.Types.ObjectId]},
        saldoDebe : { type: Number, default: 0},
        saldoHaber : { type: Number, default: 0},
        fecha: {type: Date}
      }]
    }]
});

var Cuenta = module.exports = mongoose.model('Cuenta', CuentaSchema);

module.exports.getCuentas = function( callback ){
  Cuenta.find(callback);
}

module.exports.createCuenta = function(newCuenta, callback){
  newCuenta.save(callback);
}

module.exports.getCuentaById = function(id, callback){
  Cuenta.findById(id, callback);
}

module.exports.activarCuentaById = function(id, estado, callback){
  Cuenta.update({ _id: id }, { estado: estado}, {}, callback );
}

module.exports.actualizar = function(nombre, descripcion, tipo, codigo, padre, callback){
  var condicion = { codigo : codigo };
  var update = { $set: {
    nombre: nombre,
    descripcion: descripcion,
    tipoCuenta: tipo,
    cuentaPadre : {
      padre_id: padre,
      padre_nombre: padre
    }}};
  var opciones = { upsert: true };
  Cuenta.update( condicion, update, opciones, callback);
}

module.exports.getCuentasActivas = function(callback){
  Cuenta.find({ estado: true }, callback);
}
