// core angularjs
var sic = angular.module('sic', ['ui.bootstrap']);

sic.controller('Cuentas', function($scope,$http){
  $scope._id = null;
  $scope.nombre = "";
  $scope.padre = "";
  $scope.codigo = "";
  $scope.descripcion = "";
  $scope.tipo = "";
  $scope.cuentas = [];
  $scope.tipos = [];

  $scope.cargarCuentas = function(){
    $http({
      method: 'GET',
      url: '/cuentas/catalogo'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.cuentas = data.cuentas;
        $scope.tipos = data.tipos;

      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion cuentas')
    })
  };

  $scope.guardarCuenta = function(){
    $http({
			method: 'POST',
			url: '/cuentas/nuevaCuenta',
			params: {
				nombre: $scope.nombre,
				padre: $scope.padre,
				descripcion: $scope.descripcion,
        tipo: $scope.tipo,
        codigo: $scope.codigo,
				_id: $scope._id
			}
		}).success(function(data){
      if(data == 'Ok'){
        sweetAlert("Buen trabajo!", "La cuenta ha sido guardada", "success");
        $scope.limpiarDatos();
				$scope.cargarCuentas();
			}else{
				sweetAlert("error", "Error al crear la cuenta", "error");
			}
		}).error(function(){
			alert('ERROR AL INTENTAR GUARDAR EL CUENTA');
		});
  };

  $scope.modificar = function(){
    $http({
			method: 'POST',
			url: '/cuentas/modCuenta',
			params: {
				nombre: $scope.nombre,
				padre: $scope.padre,
				descripcion: $scope.descripcion,
        tipo: $scope.tipo,
        codigo: $scope.codigo,
				_id: $scope._id
			}
		}).success(function(data){
      if(data == 'Ok'){
        sweetAlert("Buen trabajo!", "La cuenta ha sido modificada", "success");
        $scope.limpiarDatos();
				$scope.cargarCuentas();
        $('.modal').modal('hide');
			}else{
				sweetAlert("error", "Error al crear la cuenta", "error");
			}
		}).error(function(){
			alert('ERROR AL INTENTAR GUARDAR EL CUENTA');
		});
  };

  $scope.recuperarCuenta = function(indice){
    $http({
      method: 'GET',
      url: '/cuentas/recuperar',
      params:{
        _id: indice
      }
    }).success(function(data){
      if(typeof(data) == 'object'){
        $scope._id = data._id;
        $scope.nombre = data.nombre;
        $scope.padre = data.cuentaPadre.padre_nombre;
        $scope.descripcion = data.descripcion;
        $scope.tipo = data.tipoCuenta;
        $scope.codigo = data.codigo;
      }else{
        alert('ERROR AL INTENTAR RECUPERAR CUENTA');
      }
    }).error(function(){
      alert('ERROR AL INTENTAR RECUPERAR CUENTA');
    });
  };

  $scope.cambiarEstado= function(idCuenta){
    $http({
      method: 'POST',
      url: '/cuentas/activar',
      params:{
        _id: idCuenta
      }
      }).success(function(data){
        if(data == 'Ok'){
          $scope.limpiarDatos();
  				$scope.cargarCuentas();
        }else {
          alert('ERROR AL ACTIVAR LA  CUENTA');
        }
      }).error(function(){
        alert('ERROR al tratar de activar');
      });
  }
  $scope.limpiarDatos = function(){
    $scope._id = null;
    $scope.nombre = "";
    $scope.padre = "";
    $scope.descripcion = "";
    $scope.tipo = "";
    $scope.codigo = "";
	};
});
// controlador transacciones
sic.controller('Transacciones', function($scope, $http) {
  $scope.cuentas = [];
  $scope.seleccion = [];
  $scope.iva = [];
  $scope.transacciones = [];
  $scope.cuentaTrans = "";
  $scope.tipoIVA= "excento";
  $scope.iva= 0.00;
  $scope.periodo = {
    inicio: null,
    fin: null
  };
  $scope.total = {
    totalD: 0.00,
    totalH: 0.00
  };
  var Deb =0;
  var Hab = 0;
  $scope.cargarTransacciones = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/transacciones'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.transacciones = data.transacciones;
        $scope.cargarPeriodo();

      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };

  $scope.anadir= function(id){
    $http({
      method: 'GET',
      url: '/cuentas/recuperar',
      params:{
        _id: id
      }
    }).success(function(data){
      if(typeof(data) == 'object'){
        $scope.seleccion.push(data);
      }else{
        alert('ERROR AL INTENTAR RECUPERAR CUENTA');
      }
    }).error(function(){
      alert('ERROR AL INTENTAR RECUPERAR CUENTA');
    });
  };

  $scope.remover= function(sel){
    var index = $scope.seleccion.indexOf(sel);
    if( index > -1){
      $scope.seleccion.splice(index, 1);
    }
    $scope.sumar();
  };

  $scope.cuentaTran= function(sel){
    $scope.cuentaTrans = $scope.seleccion[$scope.seleccion.indexOf(sel)];
    $scope.Haber = 0.00;
    $scope.Debe = 0.00;
  };

  $scope.registrarMonto= function(){
    var ind = $scope.seleccion.indexOf($scope.cuentaTrans);
    if( ind > -1){
      $scope.seleccion[ind].desc_cuenta = {
        saldoDebe : $scope.Debe,
        saldoHaber: $scope.Haber
      }
    }
    $scope.sumar();
  };

  $scope.obtenerCuentas= function(){
    $http({
      method: 'GET',
      url: '/libroDiario/cuentas'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.cuentas = data.cuentas;
      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion cuentas')
    })
  };

  $scope.sumar = function(){
    $scope.total = {
      totalD: 0.00,
      totalH: 0.00
    };
    total = $scope.total;
    if( Object.keys($scope.seleccion).length >- 1){
      for (var i = 0; i < Object.keys($scope.seleccion).length; i++) {
        if ($scope.seleccion[i].desc_cuenta.saldoDebe) {
          $scope.total.totalD = (Number($scope.total.totalD) + Number($scope.seleccion[i].desc_cuenta.saldoDebe)).toFixed(2);
        };
        if ($scope.seleccion[i].desc_cuenta.saldoHaber) {
          $scope.total.totalH = (Number($scope.total.totalH) + Number($scope.seleccion[i].desc_cuenta.saldoHaber)).toFixed(2);
        }
      }
    }
  };

  $scope.guardarTransaccion = function(){
    for (var i = 0; i < Object.keys($scope.seleccion).length; i++) {
      if( !($scope.seleccion[i].desc_cuenta.saldoDebe || $scope.seleccion[i].desc_cuenta.saldoHaber) ) {
          $scope.seleccion.splice(i, 1);
          i =0;
      }
    }
  };

  $scope.resgistrarTransaccion = function(){
    console.log($scope.seleccion);
    $http({
      method: 'POST',
      url: '/libroDiario/registro',
      params: {
        detalle: $scope.detalle,
        fecha: $scope.fechaReg,
        seleccion: $scope.seleccion,
        tipo: "transaccion"
      }
    }).success(function(data){
      if(data == 'Ok'){
        sweetAlert("Buen trabajo!", "La transaccion ha sido creada", "success");
        $scope.cargarTransacciones();
      }else{
        sweetAlert("error", "Error al crear la transaccion", "error");
      }
    }).error(function(){
      alert('ERROR AL INTENTAR GUARDAR LA TRANSACCION');
    });
  };

  $scope.iniciarPeriodo = function(){
    $http({
      method: 'POST',
      url: '/libroDiario/addPeriodo',
      params: {
        inicio: $scope.inicio,
        final: $scope.final
      }
    }).success(function(periodo){
      if(periodo =='Ok'){
        swal("Exito","periodo guardado","success");
        $scope.cargarPeriodo();
      }else{
        swal("Opss","Esto es embarazoso", "error");
      }
    }).error(function(){
        alert("error en el sever del periodo");
      });
  };

  $scope.cargarPeriodo = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/periodoActivo',
    }).success(function(data){
      if(typeof(data) == 'object'){
        console.log(data);
        $scope.periodo.inicio = data[0].fecha_inicio;
        $scope.periodo.final = data[0].fecha_fin;
      }else{
        alert("no cargo :C");
      }
    }).error(function(){
      alert('error de coneccion')
    })
  };

  $scope.cerrarPeriodo = function(){
    $http({
      method: 'POST',
      url: '/libroDiario/registrarPeriodo',
      params: {
        transacciones: $scope.transacciones,
        periodo: $scope.periodo
      }
    }).success(function(data){
      if(data == 'Ok'){
        alert("yes");
      }else{
        alert("no cargo :C");
      }
    }).error(function(){
      alert('error de coneccion')
    })
  };

  $scope.cargarIVA = function(bandera){
    $scope.obtenerCuentas();
    $scope.iva =[];
    switch (bandera) {
      case 1:
        $scope.tipoIVA = "compras";
        for (var i = 0; i < $scope.cuentas.length; i++) {
          if($scope.cuentas[i].codigo.match(/^1105.*/)){
            $scope.iva.push($scope.cuentas[i]);
          }
        }
        break;
      case 2:
        $scope.tipoIVA = "ventas";
        for (var i = 0; i < $scope.cuentas.length; i++) {
          if($scope.cuentas[i].codigo.match(/^2107.*/)){
            $scope.iva.push($scope.cuentas[i]);
          }
        }
        console.log($scope.iva);
        break;
      default:
        $scope.tipoIVA = "excento";
    }
  };

  $scope.agregarCuentaIVA = function(cuentaIVA, nume){
    if(nume == 1){
      if($scope.tipoIVA == "compras"){
        Deb = ($scope.total.totalD* 0.13).toFixed(2);
        Hab =0;
        $scope.anadirIva(cuentaIVA._id, Hab, Deb);
      }else if ($scope.tipoIVA == "ventas") {
        Hab = ($scope.total.totalD* 0.13).toFixed(2);
        Deb =0;
        $scope.anadirIva(cuentaIVA._id, Hab, Deb);
      };
      $scope.sumar();
      //
    }else {
      $scope.anadirIva(cuentaIVA._id, Deb, Hab);
      sweetAlert('success','Ha cargado el iva con exito',"success");
      $('.modal').modal('hide');
      $scope.sumar();
    };
  };

  $scope.anadirIva= function(id, Deb, Hab){
    $http({
      method: 'GET',
      url: '/cuentas/recuperar',
      params:{
        _id: id
      }
    }).success(function(data){
      if(typeof(data) == 'object'){
        data.desc_cuenta = {
          saldoDebe : Deb,
          saldoHaber: Hab
        }
        $scope.seleccion.push(data);
        $scope.sumar();
      }else{
        alert('ERROR AL INTENTAR RECUPERAR CUENTA');
      }
    }).error(function(){
      alert('ERROR AL INTENTAR RECUPERAR CUENTA');
    });
  };

  $scope.limpiar = function(){
    $scope.seleccion = [];
    $scope.iva = [];
    $scope.detalle = " ";
    $scope.fechaReg = " ";
    $scope.cuentaTrans = "";
    $scope.tipoIVA= "excento";
    $scope.iva= 0.00;
    $scope.total = {
      totalD: 0.00,
      totalH: 0.00
    };
  }
});

//controlador de los ajustes previos al balance
sic.controller('ajustesPrevios', function($scope, $http){
  $scope.transacciones = [];
  $scope.periodo = "";
  $scope.transaccionesPreparadas = [];
  $scope.cuentasBalance = [];
  $scope.ajustesBalance = [];
  $scope.cuentas = [];
  $scope.Total = {
    inicial: {
      Debe: 0,
      Haber: 0
    },
    movimiemtos: {
      Debe: 0,
      Haber: 0
    },
    final: {
      Debe: 0,
      Haber: 0
    }
  };
  $scope.TotalAjustes = {
    inicial: {
      Debe: 0,
      Haber: 0
    },
    movimiemtos: {
      Debe: 0,
      Haber: 0
    },
    final: {
      Debe: 0,
      Haber: 0
    }
  };
  $scope.TotalFinal = {
    inicial: {
      Debe: 0,
      Haber: 0
    },
    movimiemtos: {
      Debe: 0,
      Haber: 0
    },
    final: {
      Debe: 0,
      Haber: 0
    }
  };

  $scope.tipos = [];
  $scope.cuentasAjuste = [];
  var estado = true;
  $scope.total = {
    totalD: 0.00,
    totalH: 0.00
  };
  $scope.Debe = 0;
  $scope.Haber= 0;
  $scope.periodoCerrado = false;
  $scope.tipo = "ajuste";
  $scope.transAjuste = [];

  $scope.cargarTransacciones = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/transacciones'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.transacciones = data.transacciones;
        //console.log($scope.transacciones);
      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };
  $scope.cargarPeriodo = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/periodoActivo',
    }).success(function(data){
      if(typeof(data) == 'object'){
        $scope.periodo = data[0];
      }else{
        alert("no cargo :C");
      }
    }).error(function(){
      alert('error de coneccion');
    })
  };
  $scope.cargar = function(){
    $scope.cargarTransacciones();
    $scope.cargarPeriodo();
    $scope.obtenerCuentas();

  };
  $scope.cerrarPeriodos = function(){
    $scope.transaccionesPreparadas = [];
    for (var i = 0; i < $scope.transacciones.length; i++) {
      for (var j = 0; j < $scope.transacciones[i].cuentasAfectadas.length; j++) {
        var cuenta = {
          _id: $scope.transacciones[i]._id,
          detalle: $scope.transacciones[i].detalle,
          codigo: $scope.transacciones[i].cuentasAfectadas[j].codigo,
          nombre : $scope.transacciones[i].cuentasAfectadas[j].nombre,
          inicial: {
            Debe: 0,
            Haber: 0
          },
          movimiemtos: {
            Debe: $scope.transacciones[i].cuentasAfectadas[j].Debe,
            Haber: $scope.transacciones[i].cuentasAfectadas[j].Haber
          },
          final:{
            Debe: 0,
            Haber: 0
          }
        }
        if ($scope.transacciones[i].tipo == 'transaccion') {
          $scope.transaccionesPreparadas.push(cuenta);
        }else {
          $scope.transAjuste.push(cuenta);
        }
      }
    };
    $scope.cuentasBalance = [];
    for (var i = 0; i < $scope.transaccionesPreparadas.length; i++) {
      if( $scope.cuentasBalance.length == 0 ){
        $scope.cuentasBalance.push($scope.transaccionesPreparadas[i]);
      }else {
        for (var j = 0; j < $scope.cuentasBalance.length; j++) {
          var bandera = 0;
          if($scope.cuentasBalance[j].nombre == $scope.transaccionesPreparadas[i].nombre){
            $scope.cuentasBalance[j].movimiemtos.Debe = $scope.cuentasBalance[j].movimiemtos.Debe + $scope.transaccionesPreparadas[i].movimiemtos.Debe;
            $scope.cuentasBalance[j].movimiemtos.Haber = $scope.cuentasBalance[j].movimiemtos.Haber + $scope.transaccionesPreparadas[i].movimiemtos.Haber;
            bandera = 0;
            break;
          }else {
            bandera = 1;
          }
        };
        if( bandera == 1){
          $scope.cuentasBalance.push($scope.transaccionesPreparadas[i]);
        };
        $scope.Total = {
          inicial: {
            Debe: 0,
            Haber: 0
          },
          movimiemtos: {
            Debe: 0,
            Haber: 0
          },
          final: {
            Debe: 0,
            Haber: 0
          }
        };
        for (var k = 0; k < $scope.cuentasBalance.length; k++) {
          $scope.cuentasBalance[k].final.Haber = $scope.cuentasBalance[k].movimiemtos.Haber + $scope.cuentasBalance[k].inicial.Haber;
          $scope.cuentasBalance[k].final.Debe = $scope.cuentasBalance[k].movimiemtos.Debe + $scope.cuentasBalance[k].inicial.Debe;
          if($scope.cuentasBalance[k].final.Debe > $scope.cuentasBalance[k].final.Haber){
            $scope.cuentasBalance[k].final.Debe = $scope.cuentasBalance[k].final.Debe - $scope.cuentasBalance[k].final.Haber;
            $scope.cuentasBalance[k].final.Haber = 0;
          }else{
            $scope.cuentasBalance[k].final.Haber = $scope.cuentasBalance[k].final.Haber - $scope.cuentasBalance[k].final.Debe;
            $scope.cuentasBalance[k].final.Debe = 0;
          }
          $scope.Total.inicial.Haber = $scope.Total.inicial.Haber + $scope.cuentasBalance[k].inicial.Haber;
          $scope.Total.inicial.Debe = $scope.Total.inicial.Debe + $scope.cuentasBalance[k].inicial.Debe;
          $scope.Total.movimiemtos.Haber = $scope.Total.movimiemtos.Haber + $scope.cuentasBalance[k].movimiemtos.Haber;
          $scope.Total.movimiemtos.Debe = $scope.Total.movimiemtos.Debe + $scope.cuentasBalance[k].movimiemtos.Debe;
          $scope.Total.final.Haber = $scope.Total.final.Haber + $scope.cuentasBalance[k].final.Haber;
          $scope.Total.final.Debe = $scope.Total.final.Debe + $scope.cuentasBalance[k].final.Debe;
        };
      };
    };
    for (var i = 0; i < $scope.transAjuste.length; i++) {
      if( $scope.ajustesBalance.length == 0 ){
        $scope.ajustesBalance.push($scope.transAjuste[i]);
      }else {
        for (var j = 0; j < $scope.ajustesBalance.length; j++) {
          var bandera = 0;
          if($scope.ajustesBalance[j].nombre == $scope.transAjuste[i].nombre){
            $scope.ajustesBalance[j].movimiemtos.Debe = $scope.ajustesBalance[j].movimiemtos.Debe + $scope.transAjuste[i].movimiemtos.Debe;
            $scope.ajustesBalance[j].movimiemtos.Haber = $scope.ajustesBalance[j].movimiemtos.Haber + $scope.transAjuste[i].movimiemtos.Haber;
            bandera = 0;
            break;
          }else {
            bandera = 1;
          }
        };
        if( bandera == 1){
          $scope.ajustesBalance.push($scope.transAjuste[i]);
        };
        $scope.TotalAjustes = {
          inicial: {
            Debe: 0,
            Haber: 0
          },
          movimiemtos: {
            Debe: 0,
            Haber: 0
          },
          final: {
            Debe: 0,
            Haber: 0
          }
        };
        for (var k = 0; k < $scope.ajustesBalance.length; k++) {
          $scope.ajustesBalance[k].final.Haber = $scope.ajustesBalance[k].movimiemtos.Haber + $scope.ajustesBalance[k].inicial.Haber;
          $scope.ajustesBalance[k].final.Debe = $scope.ajustesBalance[k].movimiemtos.Debe + $scope.ajustesBalance[k].inicial.Debe;
          if(Number($scope.ajustesBalance[k].final.Debe) > Number($scope.ajustesBalance[k].final.Haber)){
            $scope.ajustesBalance[k].final.Debe = $scope.ajustesBalance[k].final.Debe - $scope.ajustesBalance[k].final.Haber;
            $scope.ajustesBalance[k].final.Haber = 0;
          }else{
            $scope.ajustesBalance[k].final.Haber = $scope.ajustesBalance[k].final.Haber - $scope.ajustesBalance[k].final.Debe;
            $scope.ajustesBalance[k].final.Debe = 0;
          }
          $scope.TotalAjustes.inicial.Haber = $scope.TotalAjustes.inicial.Haber + $scope.ajustesBalance[k].inicial.Haber;
          $scope.TotalAjustes.inicial.Debe = $scope.TotalAjustes.inicial.Debe + $scope.ajustesBalance[k].inicial.Debe;
          $scope.TotalAjustes.movimiemtos.Haber = $scope.TotalAjustes.movimiemtos.Haber + $scope.ajustesBalance[k].movimiemtos.Haber;
          $scope.TotalAjustes.movimiemtos.Debe = $scope.TotalAjustes.movimiemtos.Debe + $scope.ajustesBalance[k].movimiemtos.Debe;
          $scope.TotalAjustes.final.Haber = $scope.TotalAjustes.final.Haber + $scope.ajustesBalance[k].final.Haber;
          $scope.TotalAjustes.final.Debe = $scope.TotalAjustes.final.Debe + $scope.ajustesBalance[k].final.Debe;
        };
      };
    };
    $scope.TotalFinal.inicial.Haber = $scope.TotalAjustes.inicial.Haber + $scope.Total.inicial.Haber;
    $scope.TotalFinal.inicial.Debe = $scope.TotalAjustes.inicial.Debe + $scope.Total.inicial.Debe;
    $scope.TotalFinal.movimiemtos.Haber = $scope.TotalAjustes.movimiemtos.Haber + $scope.Total.movimiemtos.Haber;
    $scope.TotalFinal.movimiemtos.Debe = $scope.TotalAjustes.movimiemtos.Debe + $scope.Total.movimiemtos.Debe;
    $scope.TotalFinal.final.Haber = $scope.TotalAjustes.final.Haber + $scope.Total.final.Haber;
    $scope.TotalFinal.final.Debe = $scope.TotalAjustes.final.Debe + $scope.Total.final.Debe;
    // console.log($scope.cuentasBalance);
    // console.log($scope.transAjuste);
    $scope.periodoCerrado = true;
  };
  $scope.obtenerCuentas= function(){
    $http({
      method: 'GET',
      url: '/cuentas/catalogo'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.cuentas = data.cuentas;
        $scope.tipos = data.tipos;
      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion cuentas')
    })
  };
  $scope.addAsiento= function(vali){
    if (vali == 2) {
      $http({
  			method: 'POST',
  			url: '/cuentas/nuevaCuenta',
  			params: {
  				nombre: $scope.nombreC,
  				padre: $scope.padreC,
  				descripcion: $scope.descripcionC,
          tipo: $scope.tipoC,
          codigo: $scope.codigoC,
  				_id: $scope.codigoC,
          estado: estado
  			}
  		}).success(function(data){
        if(data.msg == 'Ok'){
          sweetAlert("Buen trabajo!", "La cuenta ha sido guardada", "success");
          data.cuenta.desc_cuentadat.desc_cuenta = {
            saldoDebe : $scope.DebeC,
            saldoHaber : $scope.HaberC
          };
          $scope.cuentasAjuste.push(data.cuenta);
          $scope.sumar();
  			}else{
  				sweetAlert("error", "Error al crear la cuenta", "error");
  			}
  		}).error(function(){
  			alert('ERROR AL INTENTAR GUARDAR EL CUENTA');
  		});
    }else if (vali == 1){
      $http({
        method: 'GET',
        url: '/cuentas/recuperar',
        params:{
          _id: $scope.idd
        }
      }).success(function(dat){
        if(typeof(dat) == 'object'){
          dat.desc_cuenta= {
            saldoDebe : $scope.DebeC,
            saldoHaber : $scope.HaberC
          };
          $scope.cuentasAjuste.push(dat);
          $scope.sumar();
        }else{
          alert('ERROR AL INTENTAR RECUPERAR CUENTA');
        }
      }).error(function(){
        alert('ERROR AL INTENTAR RECUPERAR CUENTA');
      });
    }else {
      alert("no entro");
    }
  };
  $scope.sumar = function(){
    $scope.total = {
      totalD: 0.00,
      totalH: 0.00
    };
    total = $scope.total;
    if( Object.keys($scope.cuentasAjuste).length >- 1){
      for (var i = 0; i < Object.keys($scope.cuentasAjuste).length; i++) {
        if ($scope.cuentasAjuste[i].desc_cuenta.saldoDebe) {
          $scope.total.totalD = (Number($scope.total.totalD) + Number($scope.cuentasAjuste[i].desc_cuenta.saldoDebe)).toFixed(2);
        };
        if ($scope.cuentasAjuste[i].desc_cuenta.saldoHaber) {
          $scope.total.totalH = (Number($scope.total.totalH) + Number($scope.cuentasAjuste[i].desc_cuenta.saldoHaber)).toFixed(2);
        }
      }
    }
  };
  $scope.remover= function(sel){
    var index = $scope.cuentasAjuste.indexOf(sel);
    if( index > -1){
      $scope.cuentasAjuste.splice(index, 1);
    }
    $scope.sumar();
  };
  $scope.guardarPeriodo = function(){
    $http({
      method: 'POST',
      url: '/transacciones/cerrarPeriodo',
      params: {
        cuentas: $scope.cuentasBalance
      }
    }).success(function(data){
      if(data.msg == 'Ok'){
        sweetAlert("Buen trabajo!", "La cuenta ha sido guardada", "success");
        data.cuenta.desc_cuenta.saldoDebe = $scope.DebeC;
        data.cuenta.desc_cuenta.saldoHaber = $scope.HaberC;
        $scope.cuentasAjuste.push(data.cuenta);
        $scope.sumar();
      }else{
        sweetAlert("error", "Error al crear la cuenta", "error");
      }
    }).error(function(){
      alert('ERROR AL INTENTAR GUARDAR EL CUENTA');
    });
  };
  $scope.resgistrarAsiento= function(){
    $http({
      method: 'POST',
      url: '/libroDiario/registro',
      params: {
        detalle: $scope.detalle,
        fecha: $scope.periodo.fecha_fin,
        seleccion: $scope.cuentasAjuste,
        tipo: $scope.tipo
      }
    }).success(function(data){
      if(data == 'Ok'){
        sweetAlert("Buen trabajo!", "El ajuste ha sido creada", "success");
      }else{
        sweetAlert("error", "Error al crear el ajuste", "error");
      }
    }).error(function(){
      alert('ERROR AL INTENTAR GUARDAR LA TRANSACCION');
    });
  };
  $scope.confirmacion= function(){
    swal({
      title: "Esta seguro?",
      text: "Esta por finalizar el corriente periodo!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "terminar periodo!",
      cancelButtonText: "seguir en ajustes!",
      closeOnConfirm: false,
      closeOnCancel: true },
      function(isConfirm){
        if (isConfirm) {
          $http({
            method: 'POST',
            url: '/libroDiario/cerrar',
            params: {
            }
          }).success(function(data){
            if(data == 'Ok'){
              console.log(data);
              swal({
                title: "Ir a balance de comprobacion",
                text: "click en ok para seguir",
                type: "info",
                showCancelButton: false,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
              }, function(){
                setTimeout(function(){
                  window.location.href = "/reportes/balanceCom";
                }, 2000);
              });
            }else{
              swal("Opss", "error en el cierre del periodo", "error");
            }
          }).error(function(){
            alert('error de coneccion');
          })
        }
      }
    );
  };
  $scope.finalizarPeriodo= function(){
    $http({
      method: 'POST',
      url: '/libroDiario/cerrar',
      params: {

      }
    }).success(function(data){
      if(typeof(data) == 'object'){
        $scope.periodo = data[0];
      }else{
        alert("no cargo :C");
      }
    }).error(function(){
      alert('error de coneccion');
    })
  };
});

sic.controller('balanceComprobacion', function($scope, $http){
  $scope.transacciones = [];
  $scope.periodo = "";
  $scope.transaccionesPreparadas = [];
  $scope.cuentasBalance = [];
  $scope.ajustesBalance = [];
  $scope.cuentas = [];
  $scope.Total = {
    inicial: {
      Debe: 0,
      Haber: 0
    },
    movimiemtos: {
      Debe: 0,
      Haber: 0
    },
    final: {
      Debe: 0,
      Haber: 0
    }
  };
  $scope.TotalAjustes = {
    inicial: {
      Debe: 0,
      Haber: 0
    },
    movimiemtos: {
      Debe: 0,
      Haber: 0
    },
    final: {
      Debe: 0,
      Haber: 0
    }
  };
  $scope.TotalFinal = {
    inicial: {
      Debe: 0,
      Haber: 0
    },
    movimiemtos: {
      Debe: 0,
      Haber: 0
    },
    final: {
      Debe: 0,
      Haber: 0
    }
  };

  $scope.tipos = [];
  $scope.cuentasAjuste = [];
  var estado = true;
  $scope.total = {
    totalD: 0.00,
    totalH: 0.00
  };
  $scope.Debe = 0;
  $scope.Haber= 0;
  $scope.periodoCerrado = false;
  $scope.tipo = "ajuste";
  $scope.transAjuste = [];

  $scope.cargarTransacciones = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/transacciones'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.transacciones = data.transacciones;
        //console.log($scope.transacciones);
      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };
  $scope.cargarPeriodo = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/periodoActivo',
    }).success(function(data){
      if(typeof(data) == 'object'){
        $scope.periodo = data[0];
      }else{
        alert("no cargo :C");
      }
    }).error(function(){
      alert('error de coneccion');
    })
  };
  $scope.cargar = function(){
    $scope.cargarTransacciones();
    $scope.cargarPeriodo();

  };
  $scope.cerrarPeriodos = function(){
    $scope.transaccionesPreparadas = [];
    for (var i = 0; i < $scope.transacciones.length; i++) {
      for (var j = 0; j < $scope.transacciones[i].cuentasAfectadas.length; j++) {
        var cuenta = {
          _id: $scope.transacciones[i]._id,
          detalle: $scope.transacciones[i].detalle,
          codigo: $scope.transacciones[i].cuentasAfectadas[j].codigo,
          nombre : $scope.transacciones[i].cuentasAfectadas[j].nombre,
          inicial: {
            Debe: 0,
            Haber: 0
          },
          movimiemtos: {
            Debe: $scope.transacciones[i].cuentasAfectadas[j].Debe,
            Haber: $scope.transacciones[i].cuentasAfectadas[j].Haber
          },
          final:{
            Debe: 0,
            Haber: 0
          }
        }
        $scope.transaccionesPreparadas.push(cuenta);
      }
    };
    $scope.cuentasBalance = [];
    for (var i = 0; i < $scope.transaccionesPreparadas.length; i++) {
      if( $scope.cuentasBalance.length == 0 ){
        $scope.cuentasBalance.push($scope.transaccionesPreparadas[i]);
      }else {
        for (var j = 0; j < $scope.cuentasBalance.length; j++) {
          var bandera = 0;
          if($scope.cuentasBalance[j].nombre == $scope.transaccionesPreparadas[i].nombre){
            $scope.cuentasBalance[j].movimiemtos.Debe = $scope.cuentasBalance[j].movimiemtos.Debe + $scope.transaccionesPreparadas[i].movimiemtos.Debe;
            $scope.cuentasBalance[j].movimiemtos.Haber = $scope.cuentasBalance[j].movimiemtos.Haber + $scope.transaccionesPreparadas[i].movimiemtos.Haber;
            bandera = 0;
            break;
          }else {
            bandera = 1;
          }
        };
        if( bandera == 1){
          $scope.cuentasBalance.push($scope.transaccionesPreparadas[i]);
        };
        $scope.Total = {
          inicial: {
            Debe: 0,
            Haber: 0
          },
          movimiemtos: {
            Debe: 0,
            Haber: 0
          },
          final: {
            Debe: 0,
            Haber: 0
          }
        };
        for (var k = 0; k < $scope.cuentasBalance.length; k++) {
          $scope.cuentasBalance[k].final.Haber = $scope.cuentasBalance[k].movimiemtos.Haber + $scope.cuentasBalance[k].inicial.Haber;
          $scope.cuentasBalance[k].final.Debe = $scope.cuentasBalance[k].movimiemtos.Debe + $scope.cuentasBalance[k].inicial.Debe;
          if(Number($scope.cuentasBalance[k].final.Debe) > Number($scope.cuentasBalance[k].final.Haber)){
            $scope.cuentasBalance[k].final.Debe = $scope.cuentasBalance[k].final.Debe - $scope.cuentasBalance[k].final.Haber;
            $scope.cuentasBalance[k].final.Haber = 0;
          }else{
            $scope.cuentasBalance[k].final.Haber = $scope.cuentasBalance[k].final.Haber - $scope.cuentasBalance[k].final.Debe;
            $scope.cuentasBalance[k].final.Debe = 0;
          }
          $scope.Total.inicial.Haber = $scope.Total.inicial.Haber + $scope.cuentasBalance[k].inicial.Haber;
          $scope.Total.inicial.Debe = $scope.Total.inicial.Debe + $scope.cuentasBalance[k].inicial.Debe;
          $scope.Total.movimiemtos.Haber = $scope.Total.movimiemtos.Haber + $scope.cuentasBalance[k].movimiemtos.Haber;
          $scope.Total.movimiemtos.Debe = $scope.Total.movimiemtos.Debe + $scope.cuentasBalance[k].movimiemtos.Debe;
          $scope.Total.final.Haber = $scope.Total.final.Haber + $scope.cuentasBalance[k].final.Haber;
          $scope.Total.final.Debe = $scope.Total.final.Debe + $scope.cuentasBalance[k].final.Debe;
        };
      };
    };
    $scope.periodoCerrado = true;
  };
  $scope.print = function(){
    window.print()
  }
});

sic.controller('inventario', function($scope, $http){
  $scope.articulos = [];
  $scope.proveedores = [];
  $scope.total= 0;
  $scope.cargar = function(){
    $http({
      method: 'GET',
      url: '/inventario/cargar'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.articulos = data.articulos;
        $scope.proveedores = data.proveedores;
        $scope.limpiar();
        $scope.total= 0;
        for (var i = 0; i < $scope.articulos.length; i++) {
          $scope.total= ($scope.articulos[i].existencia*$scope.articulos[i].precioCosto) + $scope.total;
        }
        //console.log($scope.transacciones);
      }else{
        alert("nada :C");
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };
  $scope.addProveedor= function(){
    $http({
      method: 'POST',
      url: '/inventario/addProveedor',
      params: {
        nombre: $scope.proNombre,
        telefono: $scope.proDescripcion,
        contacto: $scope.proContacto
      }
    }).success(function(data){
      if(data == 'Ok'){
        swal('success',"agrego el proveedor con exito", "success");
        $scope.limpiar();
        $scope.cargar();
        // $scope.articulos = data.articulos;
        // $scope.proveedores = data.proveedores;
        //console.log($scope.transacciones);
      }else{
        alert("nada :C");
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };
  $scope.addArticulo= function(){
    $http({
      method: 'POST',
      url: '/inventario/addArticulo',
      params: {
        nombre: $scope.artNombre,
        descripcion: $scope.artDescripcion,
        proveedor: $scope.artProveedor,
        codigo: $scope.artCodigo,
        precioV: $scope.artVenta,
        precioC: $scope.artCosto,
        cantidad: $scope.artExistencia
      }
    }).success(function(data){
      if(data == 'Ok'){
        swal('success',"agrego el producto con exito", "success");
        $scope.limpiar();
        $scope.cargar();
        // $scope.articulos = data.articulos;
        // $scope.proveedores = data.proveedores;
        //console.log($scope.transacciones);
      }else{
        alert("nada :C");
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };
  $scope.limpiar = function(){
    $scope.artNombre = "";
    $scope.artDescripcion= "";
    $scope.artCodigo= "",
    $scope.artProveedor= "";
    $scope.artVenta= 0;
    $scope.artCosto= 0;
    $scope.artExistencia= 0;
    $scope.proNombre = "";
    $scope.proDescripcion= "";
    $scope.proContacto= "";
    $scope.adCosto = 0;
    $scope.adCantidad = 0;
    $scope.adNombre="";
    $scope.reqExistencia = 0;
  }

  $scope.regMovimiento= function(bandera){
    $http({
      method: 'GET',
      url: '/inventario/recuperarArticulo',
      params: {
        idea: $scope.adNombre
      }
    }).success(function(data){
      if(typeof(data)== 'object'){
        var unidades = data.existencia;
        var costo = data.precioCosto;
        var nuevaCantidad= 0;
        if (bandera == 1) {
          var nuevoCosto = 0;
          if( $scope.adCosto == 0 || $scope.adCantidad == 0){
            swal("Error","Los datos deben de ser mayor a 0","error");
          }else if ($scope.adCosto> 0 && $scope.adCantidad> 0 ){
            nuevaCantidad = Number(unidades) + Number($scope.adCantidad);
            nuevoCosto= (unidades*costo + $scope.adCosto*$scope.adCantidad)/( nuevaCantidad );
            $scope.actualizar(nuevaCantidad, nuevoCosto, data._id);
          }else {
            swal("Opss","Algo hicistes mal", "error");
          }
        }else {
          if($scope.reqExistencia > unidades){
            swal("Error","El valor debe de ser menor","error")
          }else if ($scope.reqExistencia>0) {
            nuevaCantidad= Number(unidades) - Number($scope.reqExistencia);
            if(nuevaCantidad == 0){
              costo= 0;
              $scope.actualizar(nuevaCantidad, costo, data._id);
            }else {
              $scope.actualizar(nuevaCantidad, costo, data._id);
            }
          }else {
            swal("error","Opsss, algo salio mal","error")
          }
        }
        //console.log($scope.transacciones);
      }else{
        alert("nada :C");
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };
  $scope.actualizar= function(cantidad, costo, idArt){
    $http({
      method: 'POST',
      url: '/inventario/addMovimiento',
      params: {
        _id: idArt,
        cantidad: cantidad,
        costo: costo.toFixed(2)
      }
    }).success(function(data){
      if(data == 'Ok'){
        swal('success',"Registro exitoso", "success");
        $scope.limpiar();
        $scope.cargar();
        // $scope.articulos = data.articulos;
        // $scope.proveedores = data.proveedores;
        //console.log($scope.transacciones);
      }else{
        alert("nada :C");
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  }
});

sic.controller('planilla', function($scope, $http){
  $scope.empleados = [];
  $scope.prestaciones = [];
  $scope.planilla= [];
  $scope.totalFinal= 0;
  var planilla = [];
  $scope.cargar= function(){
    $http({
      method: 'GET',
      url: '/planilla/cargar'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.empleados = data.empleados;
        $scope.prestaciones = data.prestaciones;
        console.log($scope.empleados);
        $scope.planilla();
        //console.log($scope.transacciones);
      }else{
        alert("nada :C");
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };

  $scope.planilla= function () {
    // body...
    var total = 0;
    $scope.totalFinal= 0;
    for (var i = 0; i < $scope.empleados.length; i++) {
      var salario = {
        nombre: $scope.empleados[i].nombre,
        codigo: $scope.empleados[i].codigo,
        puesto: $scope.empleados[i].puesto,
        salario: $scope.empleados[i].salario,
        horas: $scope.empleados[i].horas_mes
      };
      saldo= ((Number(7.5)/100) * Number($scope.empleados[i].salario)).toFixed(2);
      salario.ISSS= saldo;
      saldo= ((Number(6.5)/100) * Number($scope.empleados[i].salario)).toFixed(2);
      salario.AFP= saldo;
      saldo= ((Number(1)/100) * Number($scope.empleados[i].salario)).toFixed(2);
      salario.Insaforp= saldo;
      saldo = (((((Number($scope.empleados[i].salario))/30)*15)*1.3)/12).toFixed(2);
      salario.Vacaciones= saldo;
      saldo= ((Number($scope.empleados[i].salario/30)*10)/12).toFixed(2);
      salario.Aguinaldo= saldo;
      saldo= 0;
      salario.Otras= saldo;
      total = Number($scope.empleados[i].salario) + Number(salario.ISSS) +
      Number(salario.AFP)+ Number(salario.Insaforp) + Number(salario.Vacaciones) +
      Number(salario.Aguinaldo) + Number(salario.Otras);
      salario.total= total;
      planilla.push(salario);
      $scope.totalFinal = $scope.totalFinal + total;
    }
    $scope.planilla = planilla;
  };
  $scope.cargarEmpleado = function(){
    alert("en proceso :C");
  };
  $scope.guardarEmpleado = function(){
    $http({
      method: 'POST',
      url: '/planilla/addEmpleado',
      params: {
        fechaN: $scope.fechan,
        nombre: $scope.nombree,
        dui: $scope.dui,
        direccion: $scope.direccion,
        estudio: $scope.estudio,
        situacion: $scope.situacion,
        email: $scope.email,
        telefono: $scope.telefono,
        codigo: $scope.codigoe,
        puesto: $scope.puestoe,
        horas: $scope.horase,
        salario: $scope.salario
      }
    }).success(function(data){
      if(data== 'Ok'){
        swal('Exito','Se registro el empleado', 'success');
        $scope.cargar();
        $scope.planilla();
      }else {
        swal('Ouuu','Algo salio mal','error');
      }
    }).error(function(){
      alert('Error en el server xS');
    })
  };
  $scope.registrarPrestacion= function(){
    $http({
      method: 'POST',
      url: '/planilla/addPrestacion',
      params: {
        nombre: $scope.nombrep,
        descripcion: $scope.descripcionp,
        factor: $scope.factor,
        }
    }).success(function(data){
      if(data== 'Ok'){
        swal('Exito','Se registro la prestacion', 'success');
        $scope.cargar();
        $scope.planilla();
      }else {
        swal('Ouuu','Algo salio mal','error');
      }
    }).error(function(){
      alert('Error en el server xS');
    })
  };
});

sic.controller('general', function($scope, $http){
  $scope.transacciones = [];
  $scope.periodo = "";
  $scope.transaccionesPreparadas = [];
  $scope.cuentasBalance = [];
  $scope.ajustesBalance = [];
  $scope.cuentas = [];
  $scope.Total = {
    activos: {
      Debe: 0,
      Haber: 0
    },
    pasivos: {
      Debe: 0,
      Haber: 0
    },
    patrimonio: {
      Debe: 0,
      Haber: 0
    },
    resultado: {
      Debe: 0,
      Haber: 0
    }
  };
  $scope.TotalFinal = {
    activos: {
      Debe: 0,
      Haber: 0
    },
    pasivos: {
      Debe: 0,
      Haber: 0
    },
    patrimonio: {
      Debe: 0,
      Haber: 0
    },
    resultado: {
      Debe: 0,
      Haber: 0
    }
  };

  $scope.tipos = [];
  $scope.cuentasAjuste = [];
  var estado = true;
  $scope.total = {
    totalD: 0.00,
    totalH: 0.00
  };
  $scope.Debe = 0;
  $scope.Haber= 0;
  $scope.periodoCerrado = false;
  $scope.tipo = "ajuste";
  $scope.transAjuste = [];
  $scope.resultado = {};

  $scope.obtenerCuentas= function(){
    $http({
      method: 'GET',
      url: '/cuentas/catalogo'
    }).success(function(data){
      if(typeof(data)== 'object'){
        for (var i = 0; i < data.cuentas.length; i++) {
          if( data.cuentas[i].codigo.match(/^6101/)){
            $scope.cuentas.push(data.cuentas[i]);
          }
        }
      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion cuentas')
    })
  };

  $scope.cargarTransacciones = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/transacciones'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.transacciones = data.transacciones;
        //console.log($scope.transacciones);
      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };
  $scope.cargarPeriodo = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/periodoActivo',
    }).success(function(data){
      if(typeof(data) == 'object'){
        $scope.periodo = data[0];
      }else{
        alert("no cargo :C");
      }
    }).error(function(){
      alert('error de coneccion');
    })
  };
  $scope.cargar = function(){
    $scope.cargarTransacciones();
    $scope.cargarPeriodo();
    $scope.obtenerCuentas();
  };

  $scope.cerrarPeriodos = function(){
    $scope.transaccionesPreparadas = [];
    console.log(this.cuentas);
    //Obtiene todas las transacciones
    for (var i = 0; i < $scope.transacciones.length; i++) {
      for (var j = 0; j < $scope.transacciones[i].cuentasAfectadas.length; j++) {
        var cuenta = {
          _id: $scope.transacciones[i]._id,
          detalle: $scope.transacciones[i].detalle,
          codigo: $scope.transacciones[i].cuentasAfectadas[j].codigo,
          nombre : $scope.transacciones[i].cuentasAfectadas[j].nombre,
          inicial: {
            Debe: 0,
            Haber: 0
          },
          movimiemtos: {
            Debe: $scope.transacciones[i].cuentasAfectadas[j].Debe,
            Haber: $scope.transacciones[i].cuentasAfectadas[j].Haber
          },
          final:{
            Debe: 0,
            Haber: 0
          }
        }
        switch (cuenta.codigo.charAt(0)) {
          case '1':
            cuenta.tipo= "activo";
            break;
          case '2':
            cuenta.tipo= "pasivo";
            break;
          case '3':
            cuenta.tipo= 'patrimonio';
            break;
          case '4':
            cuenta.tipo= 'resultado';
            break;
          case '5':
            cuenta.tipo= 'resultado';
            break;
          default:
        }
        $scope.transaccionesPreparadas.push(cuenta);
      }
    };
    //ya estan cargadas todas las transacciones en un arreglo
    //inicializa los valores totales
    $scope.TotalFinal = {
      activos: {
        Debe: 0,
        Haber: 0
      },
      pasivos: {
        Debe: 0,
        Haber: 0
      },
      patrimonio: {
        Debe: 0,
        Haber: 0
      },
      resultado: {
        Debe: 0,
        Haber: 0
      }
    };
    $scope.Total = {
      activos: {
        Debe: 0,
        Haber: 0
      },
      pasivos: {
        Debe: 0,
        Haber: 0
      },
      patrimonio: {
        Debe: 0,
        Haber: 0
      },
      resultado: {
        Debe: 0,
        Haber: 0
      }
    };
    $scope.cuentasBalance = [];
    //recorre todas las transacciones para calcular los saldos
    for (var i = 0; i < $scope.transaccionesPreparadas.length; i++) {
      if( $scope.cuentasBalance.length == 0 ){
        $scope.cuentasBalance.push($scope.transaccionesPreparadas[i]);
      }else {
        for (var j = 0; j < $scope.cuentasBalance.length; j++) {
          var bandera = 0;
          if($scope.cuentasBalance[j].nombre == $scope.transaccionesPreparadas[i].nombre){
            $scope.cuentasBalance[j].movimiemtos.Debe = $scope.cuentasBalance[j].movimiemtos.Debe + $scope.transaccionesPreparadas[i].movimiemtos.Debe;
            $scope.cuentasBalance[j].movimiemtos.Haber = $scope.cuentasBalance[j].movimiemtos.Haber + $scope.transaccionesPreparadas[i].movimiemtos.Haber;
            bandera = 0;
            break;
          }else {
            bandera = 1;
          }
        };
        if( bandera == 1){
          $scope.cuentasBalance.push($scope.transaccionesPreparadas[i]);
        };
        for (var k = 0; k < $scope.cuentasBalance.length; k++) {
          $scope.cuentasBalance[k].final.Haber = $scope.cuentasBalance[k].movimiemtos.Haber + $scope.cuentasBalance[k].inicial.Haber;
          $scope.cuentasBalance[k].final.Debe = $scope.cuentasBalance[k].movimiemtos.Debe + $scope.cuentasBalance[k].inicial.Debe;
          if($scope.cuentasBalance[k].final.Debe > $scope.cuentasBalance[k].final.Haber){
            $scope.cuentasBalance[k].final.Debe = $scope.cuentasBalance[k].final.Debe - $scope.cuentasBalance[k].final.Haber;
            $scope.cuentasBalance[k].final.Haber = 0;
          }else{
            $scope.cuentasBalance[k].final.Haber = $scope.cuentasBalance[k].final.Haber - $scope.cuentasBalance[k].final.Debe;
            $scope.cuentasBalance[k].final.Debe = 0;
          }
          //console.log($scope.cuentasBalance[k].tipo);
        };
        //console.log($scope.TotalFinal);
      };
    };
    //todas las transascciones estan listas con los saldos
    //suma los totales de las transacciones de cada categorias para mostrarlas en la tabla
    //sacando la cuenta resultado
    for (var i = 0; i < $scope.cuentasBalance.length; i++) {
      if ($scope.cuentasBalance[i].tipo == "resultado") {
        $scope.TotalFinal.resultado.Debe= (Number($scope.cuentasBalance[i].final.Debe) + Number($scope.TotalFinal.resultado.Debe)).toFixed(2);
        $scope.TotalFinal.resultado.Haber= (Number($scope.cuentasBalance[i].final.Haber) + Number($scope.TotalFinal.resultado.Haber)).toFixed(2);
      }
    }
    if($scope.TotalFinal.resultado.Debe != 0 || $scope.TotalFinal.resultado.Haber !=0){
      var cuenta = {
        _id: $scope.cuentas[0]._id,
        detalle: "resultados",
        codigo: $scope.cuentas[0].codigo,
        nombre : $scope.cuentas[0].nombre,
        inicial: {
          Debe: 0,
          Haber: 0
        },
        movimiemtos: {
          Debe: 0,
          Haber: 0
        },
        final:{
          Debe: $scope.TotalFinal.resultado.Debe,
          Haber: $scope.TotalFinal.resultado.Haber
        },
        tipo: "patrimonio",
      }
      $scope.cuentasBalance.push(cuenta);
    }
    for (var i = 0; i < $scope.cuentasBalance.length; i++) {
      if($scope.cuentasBalance[i].tipo == "activo"){
        $scope.TotalFinal.activos.Debe= (Number($scope.cuentasBalance[i].final.Debe) + Number($scope.TotalFinal.activos.Debe)).toFixed(2);
        $scope.TotalFinal.activos.Haber= (Number($scope.cuentasBalance[i].final.Haber) + Number($scope.TotalFinal.activos.Haber)).toFixed(2);
      }else if ($scope.cuentasBalance[i].tipo == "pasivo") {
        $scope.TotalFinal.pasivos.Debe= (Number($scope.cuentasBalance[i].final.Debe) + Number($scope.TotalFinal.pasivos.Debe)).toFixed(2);
        $scope.TotalFinal.pasivos.Haber= (Number($scope.cuentasBalance[i].final.Haber) + Number($scope.TotalFinal.pasivos.Haber)).toFixed(2);
      }else if ($scope.cuentasBalance[i].tipo == "patrimonio") {
        $scope.TotalFinal.patrimonio.Debe= (Number($scope.cuentasBalance[i].final.Debe) + Number($scope.TotalFinal.patrimonio.Debe)).toFixed(2);
        $scope.TotalFinal.patrimonio.Haber= (Number($scope.cuentasBalance[i].final.Haber) + Number($scope.TotalFinal.patrimonio.Haber)).toFixed(2);
      }
    }
    //luego de agrupar por categorias define el total de cada categoria
    //categoria activos
    if (Number($scope.TotalFinal.activos.Debe) > Number($scope.TotalFinal.activos.Haber)) {
      $scope.Total.activos.Debe = $scope.TotalFinal.activos.Debe - $scope.TotalFinal.activos.Haber;
      $scope.Total.activos.Haber = 0;
    }else if ($scope.TotalFinal.activos.Debe == $scope.TotalFinal.activos.Haber) {
      $scope.Total.activos.Debe = 0;
      $scope.Total.activos.Haber = 0;
    } else{
      $scope.Total.activos.Debe = 0;
      $scope.Total.activos.Haber = $scope.TotalFinal.activos.Haber - $scope.TotalFinal.activos.Debe ;
    }
    //categoria pasivos
    if (Number($scope.TotalFinal.pasivos.Debe) > Number($scope.TotalFinal.pasivos.Haber)) {
      $scope.Total.pasivos.Debe = $scope.TotalFinal.pasivos.Debe - $scope.TotalFinal.pasivos.Haber;
      $scope.Total.pasivos.Haber = 0;
    }else if ($scope.TotalFinal.pasivos.Debe == $scope.TotalFinal.pasivos.Haber) {
      $scope.Total.pasivos.Debe = 0;
      $scope.Total.pasivos.Haber = 0;
    } else{
      $scope.Total.pasivos.Debe = 0;
      $scope.Total.pasivos.Haber = $scope.TotalFinal.pasivos.Haber - $scope.TotalFinal.pasivos.Debe ;
    }
    //categoria patrimonio
    if (Number($scope.TotalFinal.patrimonio.Debe) > Number($scope.TotalFinal.patrimonio.Haber)) {
      $scope.Total.patrimonio.Debe = $scope.TotalFinal.patrimonio.Debe - $scope.TotalFinal.patrimonio.Haber;//$scope.TotalFinal.patrimonio.Debe - $scope.TotalFinal.patrimonio.Haber;
      $scope.Total.patrimonio.Haber = 0;
    }else if ($scope.TotalFinal.patrimonio.Debe == $scope.TotalFinal.patrimonio.Haber) {
      $scope.Total.patrimonio.Debe = 0;
      $scope.Total.patrimonio.Haber = 0;
    } else{
      $scope.Total.patrimonio.Debe = 0;
      $scope.Total.patrimonio.Haber = $scope.TotalFinal.patrimonio.Haber - $scope.TotalFinal.patrimonio.Debe ;
    }

    // if (Number($scope.TotalFinal.resultado.Debe) > Number($scope.TotalFinal.resultado.Haber)) {
    //   $scope.TotalFinal.resultado.Debe = $scope.TotalFinal.resultado.Debe - $scope.TotalFinal.resultado.Haber;
    //   $scope.TotalFinal.resultado.Haber = 0;
    // }else if ($scope.TotalFinal.resultado.Debe == $scope.TotalFinal.resultado.Haber) {
    //   $scope.TotalFinal.resultado.Debe = 0;
    //   $scope.TotalFinal.resultado.Haber = 0;
    // } else{
    //   $scope.TotalFinal.resultado.Debe = 0;
    //   $scope.TotalFinal.resultado.Haber = $scope.TotalFinal.resultado.Haber - $scope.TotalFinal.resultado.Debe ;
    // }
    console.log($scope.TotalFinal.resultado);
    //aqui deberia de cerrar el periodo
    $scope.periodoCerrado = true;
    //console.log($scope.cuentasBalance );
  };
  $scope.print = function(){
    window.print()
  };
});
sic.controller('resultado', function($scope, $http){
  $scope.transacciones = [];
  $scope.periodo = "";
  $scope.transaccionesPreparadas = [];
  $scope.cuentasResultado = [];
  $scope.Total = {
    ingresos: {
      Debe: 0,
      Haber: 0
    },
    gastos: {
      Debe: 0,
      Haber: 0
    }
  };
  $scope.TotalFinal = {
    ingresos: {
      Debe: 0,
      Haber: 0
    },
    gastos: {
      Debe: 0,
      Haber: 0
    }
  };
  $scope.periodoCerrado = false;

  $scope.cargarTransacciones = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/transacciones'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.transacciones = data.transacciones;
        //console.log($scope.transacciones);
      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };
  $scope.cargarPeriodo = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/periodoActivo',
    }).success(function(data){
      if(typeof(data) == 'object'){
        $scope.periodo = data[0];
      }else{
        alert("no cargo :C");
      }
    }).error(function(){
      alert('error de coneccion');
    })
  };
  $scope.cargar = function(){
    $scope.cargarTransacciones();
    $scope.cargarPeriodo();
  };
  $scope.cerrarPeriodos = function(){
    $scope.transaccionesPreparadas = [];
    //crea un arreglo con todas las trnasacciones y sus respectivas cuentas
    for (var i = 0; i < $scope.transacciones.length; i++) {
      for (var j = 0; j < $scope.transacciones[i].cuentasAfectadas.length; j++) {
        var cuenta = {
          _id: $scope.transacciones[i]._id,
          detalle: $scope.transacciones[i].detalle,
          codigo: $scope.transacciones[i].cuentasAfectadas[j].codigo,
          nombre : $scope.transacciones[i].cuentasAfectadas[j].nombre,
          inicial: {
            Debe: 0,
            Haber: 0
          },
          movimiemtos: {
            Debe: $scope.transacciones[i].cuentasAfectadas[j].Debe,
            Haber: $scope.transacciones[i].cuentasAfectadas[j].Haber
          },
          final:{
            Debe: 0,
            Haber: 0
          }
        }
        //valida si es ingreso o gasto, y lo agrega a cuantas
        if (cuenta.codigo.match(/^5.*/)) {
          cuenta.tipo = "ingreso";
          $scope.transaccionesPreparadas.push(cuenta);
        }else if (cuenta.codigo.match(/^4.*/)) {
          cuenta.tipo = "gasto";
          $scope.transaccionesPreparadas.push(cuenta);
        }else {
          console.log("nou");
        }
      }
    };
    $scope.cuentasResultado = [];
    $scope.TotalFinal = {
      ingresos: {
        Debe: 0,
        Haber: 0
      },
      gastos: {
        Debe: 0,
        Haber: 0
      }
    };
    //configura las cuentas con el valor final en cada cuenta, suma transacciones y calcula el final
    for (var i = 0; i < $scope.transaccionesPreparadas.length; i++) {
      if( $scope.cuentasResultado.length == 0 ){
        $scope.cuentasResultado.push($scope.transaccionesPreparadas[i]);
      }else {
        //setea las cuentas ncecesarias
        for (var j = 0; j < $scope.cuentasResultado.length; j++) {
          var bandera = 0;
          if($scope.cuentasResultado[j].nombre == $scope.transaccionesPreparadas[i].nombre){
            $scope.cuentasResultado[j].movimiemtos.Debe = $scope.cuentasResultado[j].movimiemtos.Debe + $scope.transaccionesPreparadas[i].movimiemtos.Debe;
            $scope.cuentasResultado[j].movimiemtos.Haber = $scope.cuentasResultado[j].movimiemtos.Haber + $scope.transaccionesPreparadas[i].movimiemtos.Haber;
            bandera = 0;
            break;
          }else {
            bandera = 1;
          }
        };
        if( bandera == 1){
          $scope.cuentasResultado.push($scope.transaccionesPreparadas[i]);
        };
        // Obtiene el valor final de la suma de saldos iniciales mas movimientos

      };
    };
    for (var k = 0; k < $scope.cuentasResultado.length; k++) {
      $scope.cuentasResultado[k].final.Haber = $scope.cuentasResultado[k].movimiemtos.Haber + $scope.cuentasResultado[k].inicial.Haber;
      $scope.cuentasResultado[k].final.Debe = $scope.cuentasResultado[k].movimiemtos.Debe + $scope.cuentasResultado[k].inicial.Debe;
      if($scope.cuentasResultado[k].final.Debe > $scope.cuentasResultado[k].final.Haber){
        $scope.cuentasResultado[k].final.Debe = $scope.cuentasResultado[k].final.Debe - $scope.cuentasResultado[k].final.Haber;
        $scope.cuentasResultado[k].final.Haber = 0;
      }else{
        $scope.cuentasResultado[k].final.Haber = $scope.cuentasResultado[k].final.Haber - $scope.cuentasResultado[k].final.Debe;
        $scope.cuentasResultado[k].final.Debe = 0;
      }
    };

    //configuraciones para el estado correspondiente
    for (var i = 0; i < $scope.cuentasResultado.length; i++) {
      if($scope.cuentasResultado[i].tipo == "ingreso"){
        $scope.TotalFinal.ingresos.Debe= Number($scope.cuentasResultado[i].final.Debe) + Number($scope.TotalFinal.ingresos.Debe);
        $scope.TotalFinal.ingresos.Haber= Number($scope.cuentasResultado[i].final.Haber) + Number($scope.TotalFinal.ingresos.Haber);
        if (Number($scope.TotalFinal.ingresos.Debe) > Number($scope.TotalFinal.ingresos.Haber)) {
          $scope.Total.ingresos.Debe = $scope.TotalFinal.ingresos.Debe - $scope.TotalFinal.ingresos.Haber;
          $scope.Total.ingresos.Haber = 0;
        }else if ($scope.TotalFinal.ingresos.Debe == $scope.TotalFinal.ingresos.Haber) {
          $scope.Total.ingresos.Debe = 0;
          $scope.Total.ingresos.Haber = 0;
        } else{
          $scope.Total.ingresos.Debe = 0;
          $scope.Total.ingresos.Haber = $scope.TotalFinal.ingresos.Haber - $scope.TotalFinal.ingresos.Debe ;
        }
      }else{
        $scope.TotalFinal.gastos.Debe= (Number($scope.cuentasResultado[i].final.Debe) + Number($scope.TotalFinal.gastos.Debe)).toFixed(2);
        $scope.TotalFinal.gastos.Haber= (Number($scope.cuentasResultado[i].final.Haber) + Number($scope.TotalFinal.gastos.Haber)).toFixed(2);

        if (Number($scope.TotalFinal.gastos.Debe) > Number($scope.TotalFinal.gastos.Haber)) {
          $scope.Total.gastos.Debe = $scope.TotalFinal.gastos.Debe - $scope.TotalFinal.gastos.Haber;
          $scope.Total.gastos.Haber = 0;
        }else if ($scope.TotalFinal.gastos.Debe == $scope.TotalFinal.gastos.Haber) {
          $scope.Total.gastos.Debe = 0;
          $scope.Total.gastos.Haber = 0;
        } else{
          $scope.Total.gastos.Debe = 0;
          $scope.Total.gastos.Haber = $scope.TotalFinal.gastos.Haber - $scope.TotalFinal.gastos.Debe ;
        }
      };
    };
    $scope.periodoCerrado = true;
    //console.log($scope.cuentasResultado );
  };
  $scope.print = function(){
    window.print()
  };
});

sic.controller('capital', function($scope, $http){
  $scope.transacciones = [];
  $scope.periodo = "";
  $scope.transaccionesPreparadas = [];
  $scope.cuentasCapital = [];
  $scope.cuentas = [];
  $scope.Total = {
      Debe: 0,
      Haber: 0
  };
  $scope.TotalFinal = {
      Debe: 0,
      Haber: 0
  };
  $scope.periodoCerrado = false;
  $scope.resultado = {
    Debe: 0,
    Haber: 0
  }

  $scope.obtenerCuentas= function(){
    $http({
      method: 'GET',
      url: '/cuentas/catalogo'
    }).success(function(data){
      if(typeof(data)== 'object'){
        for (var i = 0; i < data.cuentas.length; i++) {
          if( data.cuentas[i].codigo.match(/^6101/)){
            $scope.cuentas.push(data.cuentas[i]);
          }
        }
      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion cuentas')
    })
  };
  $scope.cargarTransacciones = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/transacciones'
    }).success(function(data){
      if(typeof(data)== 'object'){
        $scope.transacciones = data.transacciones;
        //console.log($scope.transacciones);
      }else{
        alert(data);
      }
    }).error(function(){
      alert('error de coneccion con las transacciones')
    })
  };
  $scope.cargarPeriodo = function(){
    $http({
      method: 'GET',
      url: '/libroDiario/periodoActivo',
    }).success(function(data){
      if(typeof(data) == 'object'){
        $scope.periodo = data[0];
      }else{
        alert("no cargo :C");
      }
    }).error(function(){
      alert('error de coneccion');
    })
  };
  $scope.cargar = function(){
    $scope.cargarTransacciones();
    $scope.cargarPeriodo();
    $scope.obtenerCuentas();
  };
  $scope.cerrarPeriodos = function(){
    $scope.transaccionesPreparadas = [];
    //crea un arreglo con todas las trnasacciones y sus respectivas cuentas
    for (var i = 0; i < $scope.transacciones.length; i++) {
      for (var j = 0; j < $scope.transacciones[i].cuentasAfectadas.length; j++) {
        var cuenta = {
          _id: $scope.transacciones[i]._id,
          detalle: $scope.transacciones[i].detalle,
          codigo: $scope.transacciones[i].cuentasAfectadas[j].codigo,
          nombre : $scope.transacciones[i].cuentasAfectadas[j].nombre,
          inicial: {
            Debe: 0,
            Haber: 0
          },
          movimiemtos: {
            Debe: $scope.transacciones[i].cuentasAfectadas[j].Debe,
            Haber: $scope.transacciones[i].cuentasAfectadas[j].Haber
          },
          final:{
            Debe: 0,
            Haber: 0
          }
        }
        //valida si es ingreso o gasto, y lo agrega a cuantas
        if (cuenta.codigo.match(/^3.*/)) {
          cuenta.tipo = "patrimonio";
          $scope.transaccionesPreparadas.push(cuenta);
        }else if (cuenta.codigo.match(/^4.*/) || cuenta.codigo.match(/^5.*/)) {
          cuenta.tipo="resultado";
          $scope.transaccionesPreparadas.push(cuenta);
        }else {
          console.log("nou");
        }
      }
    };
    $scope.cuentasCapital = [];
    $scope.TotalFinal = {
        Debe: 0,
        Haber: 0
    };
    $scope.resultado = {
      Debe: 0,
      Haber: 0
    };
    //configura las cuentas con el valor final en cada cuenta, suma transacciones y calcula el final
    for (var i = 0; i < $scope.transaccionesPreparadas.length; i++) {
      if( $scope.cuentasCapital.length == 0 ){
        $scope.cuentasCapital.push($scope.transaccionesPreparadas[i]);
      }else {
        //setea las cuentas ncecesarias
        for (var j = 0; j < $scope.cuentasCapital.length; j++) {
          var bandera = 0;
          if($scope.cuentasCapital[j].nombre == $scope.transaccionesPreparadas[i].nombre){
            $scope.cuentasCapital[j].movimiemtos.Debe = $scope.cuentasCapital[j].movimiemtos.Debe + $scope.transaccionesPreparadas[i].movimiemtos.Debe;
            $scope.cuentasCapital[j].movimiemtos.Haber = $scope.cuentasCapital[j].movimiemtos.Haber + $scope.transaccionesPreparadas[i].movimiemtos.Haber;
            bandera = 0;
            break;
          }else {
            bandera = 1;
          }
        };
        if( bandera == 1){
          $scope.cuentasCapital.push($scope.transaccionesPreparadas[i]);
        };
        // Obtiene el valor final de la suma de saldos iniciales mas movimientos
      };
    };
    for (var k = 0; k < $scope.cuentasCapital.length; k++) {
      $scope.cuentasCapital[k].final.Haber = $scope.cuentasCapital[k].movimiemtos.Haber + $scope.cuentasCapital[k].inicial.Haber;
      $scope.cuentasCapital[k].final.Debe = $scope.cuentasCapital[k].movimiemtos.Debe + $scope.cuentasCapital[k].inicial.Debe;
      if($scope.cuentasCapital[k].final.Debe > $scope.cuentasCapital[k].final.Haber){
        $scope.cuentasCapital[k].final.Debe = $scope.cuentasCapital[k].final.Debe - $scope.cuentasCapital[k].final.Haber;
        $scope.cuentasCapital[k].final.Haber = 0;
      }else{
        $scope.cuentasCapital[k].final.Haber = $scope.cuentasCapital[k].final.Haber - $scope.cuentasCapital[k].final.Debe;
        $scope.cuentasCapital[k].final.Debe = 0;
      };
    };
    //configuraciones para el estado correspondiente
    for (var i = 0; i < $scope.cuentasCapital.length; i++) {
      if ($scope.cuentasCapital[i].tipo == "resultado") {
        $scope.resultado.Debe= (Number($scope.cuentasCapital[i].final.Debe) + Number($scope.resultado.Debe)).toFixed(2);
        $scope.resultado.Haber= (Number($scope.cuentasCapital[i].final.Haber) + Number($scope.resultado.Haber)).toFixed(2);
      }
    }
    if($scope.resultado.Debe != 0 || $scope.resultado.Haber !=0){
      var cuenta = {
        _id: $scope.cuentas[0]._id,
        detalle: "resultados",
        codigo: $scope.cuentas[0].codigo,
        nombre : $scope.cuentas[0].nombre,
        inicial: {
          Debe: 0,
          Haber: 0
        },
        movimiemtos: {
          Debe: 0,
          Haber: 0
        },
        final:{
          Debe: $scope.resultado.Debe,
          Haber: $scope.resultado.Haber
        },
        tipo: "patrimonio",
      }
      $scope.cuentasCapital.push(cuenta);
    }
    for (var i = 0; i < $scope.cuentasCapital.length; i++) {
      if ($scope.cuentasCapital[i].tipo == "patrimonio") {
        $scope.TotalFinal.Debe= (Number($scope.cuentasCapital[i].final.Debe) + Number($scope.TotalFinal.Debe)).toFixed(2);
        $scope.TotalFinal.Haber= (Number($scope.cuentasCapital[i].final.Haber) + Number($scope.TotalFinal.Haber)).toFixed(2);
      }
    };
    if (Number($scope.TotalFinal.Debe) > Number($scope.TotalFinal.Haber)) {
      $scope.Total.Debe = $scope.TotalFinal.Debe - $scope.TotalFinal.Haber;
      $scope.Total.Haber = 0;
    }else if ($scope.TotalFinal.Debe == $scope.TotalFinal.Haber) {
      $scope.Total.Debe = 0;
      $scope.Total.Haber = 0;
    } else{
      $scope.Total.Debe = 0;
      $scope.Total.Haber = $scope.TotalFinal.Haber - $scope.TotalFinal.Debe ;
    }
    $scope.periodoCerrado = true;
    //console.log($scope.cuentasCapital );
  };
  $scope.print = function(){
    window.print()
  };
});
