$(document).ready(function() {
    $('#Tablas1').DataTable( {
        "pagingType": "full_numbers"
         columnDefs: [ {
           targets: [ 0 ],
           orderData: [ 0, 1 ]
         }, {
           targets: [ 1 ],
           orderData: [ 1, 0 ]
         }, {
           targets: [ 4 ],
           orderData: [ 4, 0 ]
         } ]
       } );
       "columnDefs": [
           {
               "targets": [ 2 ],
               "visible": false,
               "searchable": false
           },
           {
               "targets": [ 3 ],
               "visible": false
           }
       ]
       "dom": '<"top"i>rt<"bottom"flp><"clear">'
   } );
