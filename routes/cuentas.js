var express = require('express');
var router = express.Router();

//modelos
var Cuenta = require('../models/cuenta/cuenta');
var Tipo =require('../models/cuenta/tipo');
var Pago = require('../models/cuenta/pago');
var Transaccion = require('../models/cuenta/transaccion');
var Periodo = require('../models/cuenta/periodo');

// principal /cuentas/
router.get('/', function(req, res, next) {
  Cuenta.getCuentas(function(err, cuentas){
    if(err){
      console.log(err);
      res.send(err);
    } else {
      res.send({
        'title': 'Catalogo de cuentas',
        "cuentas" : cuentas
      });
    }
  });
});

//Routers de las cuentas
router.get('/gestionarCuentas', function(req, res, next){
  Cuenta.getCuentas(function(err, cuentas){
    if(err){
      console.log(err);
      res.send(err);
    }else{
      res.render('general/gestionarCuentas');
    }
  })
});
// obtener las cuentas
router.get('/catalogo', function(req, res, next){
  Cuenta.getCuentas(function(err, cuentas){
    if(err){
      res.send(err);
    }else{
      Tipo.getTipos(function(error, tipos){
        if(error){
          res.send(error);
        }else {
          res.send({"cuentas": cuentas, "tipos":tipos});
        }
    });
    }
  })
});
//obtener una cuenta
router.get('/recuperar', function(req, res, next){
  Cuenta.getCuentaById(req.query._id, function(err, cuenta){
    if(err){
      res.send("nope");
    }else{
      res.send(cuenta);
    }
  })
});

// Agrega un nueva cuenta a la base de datos
router.post('/nuevaCuenta', function(req, res, next){
  var nombre = req.query.nombre;
  var descripcion = req.query.descripcion;
  var padre = req.query.padre;
  var tipo = req.query.tipo;
  var id = req.query._id;
  var codigo = req.query.codigo;
  var estado = false;
  if(req.query.estado){
    estado = true;
  }
// Validar en formulario
  req.checkQuery('nombre','campo nombre requerido').notEmpty();
  req.checkQuery('descripcion','campo descripcion requerido').notEmpty();
  req.checkQuery('padre','campo categoria requerido').notEmpty();
  req.checkQuery('tipo','el tipo de cuenta es requerido').notEmpty();
  req.checkQuery('codigo','el codigo es requerido').notEmpty();
//comprobar errores
  var errors = req.validationErrors();
  if(errors){
        res.send(errors);
  }else{
    var newCuenta = new Cuenta({
      nombre: nombre,
      descripcion: descripcion,
      tipoCuenta: tipo,
      codigo: codigo,
      estado: estado,
      cuentaPadre : {
        padre_id: padre,
        padre_nombre: padre
      }
    });
    //creando la cuenta
    Cuenta.createCuenta(newCuenta, function(err, cuent){
      if(err) throw err;
      console.log(cuent);
    });
    if(estado == true){
      res.send({
        msg: 'Ok',
        cuenta: newCuenta
      })
    }else {
      res.send('Ok');
    }
  }
});

router.post('/modCuenta', function(req, res){
  var nombre = req.query.nombre;
  var descripcion = req.query.descripcion;
  var padre = req.query.padre;
  var tipo = req.query.tipo;
  var codigo = req.query.codigo;
// Validar en formulario
  req.checkQuery('nombre','campo nombre requerido').notEmpty();
//comprobar errores
  var errors = req.validationErrors();
  if(errors){
        res.send(errors);
  }else{
    //actualizar la cuenta
    Cuenta.actualizar( nombre, descripcion, tipo, codigo, padre, function(err, cuent){
      if(err) throw err;
      console.log("error");
    });
    res.send('Ok');  
  }
});

//Activando la cuenta
router.post('/activar', function(req, res, next){
  var _id = req.query._id;
  var estado = false;
  Cuenta.getCuentaById(_id, function(err, cuenta){
    if(err){
      res.send(err);
    }else{
      if(cuenta.estado == false){
        estado = true;
      }else {
        estado = false;
      }
      Cuenta.activarCuentaById(_id, estado, function(err, estado){
        if(err) throw err;
      });
      res.send('Ok');
    }
  });
});

module.exports = router;
