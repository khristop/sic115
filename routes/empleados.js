var express = require('express');
var router = express.Router();

//models

var Empleado = require('../models/empleados/empleado');
var Prestacion = require('../models/empleados/prestacion');
//rutas

router.get('/', function(req, res, next){
    res.render('costos/Planillas');
});

router.post('/addEmpleado', function(req, res){
  newEmpleado = new Empleado({
    nombre            : req.query.nombre,
    dui               : req.query.dui,
    direccion         : req.query.direccion,
    fecha_nacimiento  : req.query.fechaN,
    nivel_academico   : req.query.estudio,
    situacion         : req.query.situacion,
    email             : req.query.email,
    telefono          : req.query.telefono,
    codigo            : req.query.codigo,
    puesto            : req.query.puesto,
    horas_mes         : req.query.horas,
    salario           : req.query.salario
  });
  Empleado.createEmpleado(newEmpleado, function(err, Empleado){
    if(err) throw err;
  });
  res.send('Ok');
});

router.post('/addPrestacion', function(req, res){
  newPrestacion = new Prestacion({
    nombre: req.query.nombre,
    descripcion: req.query.descripcion,
    factor: req.query.factor
  });
  Prestacion.createPrestacion(newPrestacion, function(err, prestacion){
    if(err){
      res.send(err);
    }else {
      res.send('Ok')
    }
  });
});

router.get('/cargar', function(req, res){
  Empleado.getEmpleados(function(err, empleados){
    if(err){
      res.send(err);
    }else {
      Prestacion.getPrestaciones(function(err, Prestaciones){
        if(err){
          res.send(err);
        }else {
          res.send({
            empleados : empleados,
            prestaciones : Prestaciones
          });
        }
      })
    }
  })
});
module.exports = router;
