var express = require('express');
var router = express.Router();

//modelos
var Proveedor = require('../models/inventario/proveedor');
var Articulo = require('../models/inventario/articulo');

//rutas

router.get('/', function(req, res){
  res.render('costos/inventario');
});

router.get('/cargar', function(req, res){
  Proveedor.getProveedores( function(err, proveedores){
    if(err){
      res.send(err);
    }else {
      Articulo.getArticulos( function(err, articulos){
        if(err){
          res.send(err);
        }else {
          res.send({
            articulos: articulos,
            proveedores: proveedores
          })
        }
      })
    }
  })
});

router.post('/addArticulo', function(req, res) {
  // body...
  Proveedor.getProveedorById(req.query.proveedor, function(err, proveedor){
    newArticulo = new Articulo({
      nombre: req.query.nombre,
      descripcion: req.query.descripcion,
      codigo: req.query.codigo,
      precioVenta: req.query.precioV,
      precioCosto: req.query.precioC,
      existencia: req.query.cantidad,
      proveedor: {
        proveedorId: proveedor._id,
        nombre: proveedor.nombre
      }
    });
    Articulo.createArticulo( newArticulo, function(err, articulo){
      if (err) throw err;
    });
    res.send('Ok');
  })
});

router.get('/recuperarArticulo', function(req, res, next){
  Articulo.getArticuloById(req.query.idea, function(err, articulo){
    if(err){
      res.send(err);
    }else {
      res.send(articulo);
    }
  })
});

router.post('/addProveedor', function(req, res){
  newProveedor = new Proveedor({
    nombre: req.query.nombre,
    telefono: req.query.telefono,
    contacto: req.query.contacto
  })
  Proveedor.createProveedor(newProveedor, function(err, proveedor){
    if(err) throw err;
  });
  res.send('Ok');
});

router.post('/addMovimiento', function(req, res){
  Articulo.updateArticulo(req.query._id, req.query.cantidad, req.query.costo, function(err, articulo){
    if(err){
      res.send(err);
    }else {
      res.send("Ok");
      console.log(articulo);
    }
  })
})

module.exports = router;
