var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

//modelos
var User = require('../models/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/register', function(req, res, next) {
  res.render('register',{
    'title': 'Registrarse',
  })
});

router.get('/login', function(req, res, next) {
  res.render('login',{
    'title': 'Login',
  })
});

// Registrarse en el sistema
router.post('/register', function(req, res, next){
  //Obtener los valores de el formulario
  var name= req.body.name;
  var email = req.body.email;
  var username = req.body.username;
  var password = req.body.password;
  var password2 = req.body.password2;
  //comprobar el archivo de Imagen
  //console.log("hola"+req.files.image+"hola");
  if(req.files.image){
    console.log('Uploading files ...');
    var imageOriginalName = req.files.image.originalname;
    var imageName = req.files.image.name;
    var imageMime = req.files.image.mimetype;
    var imagePath = req.files.image.path;
    var imageExt = req.files.image.extension;
    var imageSize = req.files.image.size;
  } else{
    //Set default Imagen
    var imageName = 'noimage.png';
    console.log('no imagen');
  }

  // Validar formulario
  req.checkBody('name','Campo nombre es requerido').notEmpty();
  req.checkBody('email','Campo email es requerido').notEmpty();
  req.checkBody('email','Email no valido').isEmail();
  req.checkBody('username','Campo usuario es requerido').notEmpty();
  req.checkBody('password','Campo password es requerido').notEmpty();
  req.checkBody('password2','Las contraseñas no son iguales').equals(req.body.password);

  //check errores
  var errors = req.validationErrors();

  if(errors){
    res.render('register',{
      errors: errors,
      name: name,
      email: email,
      username: username,
      password: password,
      password2: password2
    });
  }else{
    var newUser = new User({
      name: name,
      email: email,
      username: username,
      password: password,
      image: imageName
    });

    // Create User
    User.createUser(newUser, function(err, user){
      if(err) throw err;
      console.log(user);
    });

    //mensaje de exito
    req.flash('success', 'Ahora estas registrado, puedes acceder con tu nombre de usuario y contraseña');

    res.location('/');
    res.redirect('/');
  }
});

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});

passport.use(new LocalStrategy(
  function(username, password, done){
    User.getUserByUsername(username, function(err, user){
      if(err) throw err;
      if(!user){
        console.log('Usuario desconocido');
        return done(null, false,{message: 'Usuario desconocido'});
      }

      User.comparePassword(password, user.password, function(err, isMatch){
        if(err) throw err;
        if(isMatch){
          return done(null, user);
        } else {
          console.log('Invalid Password');
          return done(null, false, {message: 'Invalid Password'});
        }
      });
    });
  }
));

router.post('/login', passport.authenticate('local', {failureRedirect:'/users/login', failureFlash:'Usuario Invalido o contraseña'}), function(req, res){
  console.log('Autentificacion completada');
  req.flash('success','Tu estas logueado');
  res.redirect('/');
});

router.get('/logout', function(req, res){
  req.logout();
  req.flash('success','Te has desconectado');
  res.redirect('/users/login');
});

module.exports = router;
