var express = require('express');
var router = express.Router();
//modelos
var Cuenta = require('../models/cuenta/cuenta');
var Tipo =require('../models/cuenta/tipo');
var Pago = require('../models/cuenta/pago');
var Transaccion = require('../models/cuenta/transaccion');
var Periodo = require('../models/cuenta/periodo');

//
router.get('/', function(req, res, next){
  res.send("Estos son los reportes")
});
//ajustes Previos
router.get('/ajustes', function(req, res){
  res.render('costos/ajustesPrevios');
});

router.get('/ajustes/cerrarPeriodo', function(req, res){
  // var transaccion =[];
  // Transaccion.getTransacciones(function(err, transacciones){
  //   if(err){
  //     console.log(err);
  //   }else{
  //     transaccion = transacciones;
  //   }
  //   res.send(transaccion);
  // })
  // console.log(transaccion);

});
//Balance de comprobacion
router.get('/balanceCom', function(req, res){
  res.render('reportes/balanceCom');
});

router.get('/BResultado', function(req, res){
  res.render('reportes/BResultado');
});

router.get('/EsCapital', function(req, res){
  res.render('reportes/EsCapital');
});

router.get('/BalanceGen', function(req, res){
  res.render('reportes/BalanceGen');
});


module.exports = router;
