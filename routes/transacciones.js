var express = require('express');
var router = express.Router();

//modelos
var Cuenta = require('../models/cuenta/cuenta');
var Tipo =require('../models/cuenta/tipo');
var Pago = require('../models/cuenta/pago');
var Transaccion = require('../models/cuenta/transaccion');
var Periodo = require('../models/cuenta/periodo');

//
router.get('/', function(req, res, next){
    res.render('general/libroDiario');
})

router.get('/cuentas', function(req, res, next){
  Cuenta.getCuentasActivas(function(err, cuentas){
    if(err){
      res.send(err);
    }else{
      res.send({"cuentas": cuentas});
      }
  })
});

router.post('/registro', function(req, res){
  detalle= req.query.detalle;
  fecha_creacion = req.query.fecha;
  tipo= req.query.tipo;
  cuentas = req.query.seleccion;
  var monto = 0 ;
  newTransaccion = new Transaccion({
    detalle: detalle,
    fecha_creacion: fecha_creacion,
    tipo: tipo
  });
  for (var i = 0; i < cuentas.length; i++) {
    var cuentaT =  JSON.parse(cuentas[i]);
    var afectada = {
      _id: cuentaT._id,
      nombre: cuentaT.nombre,
      codigo: cuentaT.codigo,
      Debe: cuentaT.desc_cuenta.saldoDebe,
      Haber: cuentaT.desc_cuenta.saldoHaber
    }
    monto = parseFloat(cuentaT.desc_cuenta.saldoDebe) + monto;
    newTransaccion.cuentasAfectadas.push(afectada);
  };
  newTransaccion.monto = monto;
  Transaccion.guardarTransaccion( newTransaccion, function(err, transaccion){
     if(err) throw err;
     console.log(transaccion);
  });
  res.send('Ok');
});

router.get('/transacciones', function(req, res){
  Transaccion.getTransacciones(function(err, transacciones){
    if(err){
      res.send(err);
    }else{
      res.send({"transacciones": transacciones});
    }
  })
});

router.post('/addPeriodo', function(req, res){
  neWperiodo = new Periodo({
    fecha_inicio: req.query.inicio,
    fecha_fin: req.query.final,
    estado: true
  })
  Periodo.iniciarPeriodo(neWperiodo, function(err, periodo){
    if(err) throw error;
  });
  res.send("Ok");
});

router.get('/periodoActivo', function(req, res){
  Periodo.periodoActivo( function(err, periodo){
    if(err){
      res.send(err);
    }else{
      res.send(periodo);
    }
  });
});

router.post('/registrarPeriodo', function(req, res){
  console.log(req.query.transacciones);
  res.send("Ok");
});

// Cerrando el periodo
router.post('/cerrar', function(req, res){
  res.send("Ok");
})

module.exports = router;
